var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var FOODY = {
    // _API: "https://publicapi.vienthonga.vn",
    // _URL: "https://vienthonga.vn"
};

var Layout = (function () {

    var setUserAgent = function () {

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            bodyElement.classList.add("window-phone");
            return;
        }

        if (/android/i.test(userAgent)) {
            bodyElement.classList.add("android");
            return;
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            bodyElement.classList.add("ios");
            return;
        }
    };


    var scrollToTop = function () {
        var scrollTop = $("#scrollToTop");

        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 200) {
                scrollTop.addClass("active");
            } else {
                scrollTop.removeClass("active");
            }
        });

        scrollTop.click(function () {
            $("body, html").animate(
                {
                    scrollTop: 0
                },
                500
            );
        });
    };

    var viewportHeight = function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);


        // We listen to the resize event
        window.addEventListener('resize', () => {
            // We execute the same script as before
            let vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
        });


    };

    return {
        init: function () {
            setUserAgent();
            scrollToTop();
            viewportHeight();
        }
    };
})();


$(window).on("load", function () {

    // Page loader


    $("body").imagesLoaded(function () {
        setTimeout(function (e) {
            $(".page-loader div").fadeOut();
            $(".page-loader").delay(200).fadeOut("slow");
        }, 1000)

    });


});


$(document).ready(function () {
    Layout.init();


    var hvSwiper = new Swiper('.hv-swiper', {
        slidesPerView: 4,
        spaceBetween: 0,

        breakpoints: {
            640: {
                slidesPerView: 3,
                spaceBetween: 0,
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });


    var D = $(".countdown-s2");
    D.length > 0 &&
    D.each(function () {
        var e = $(this),
            i = e.attr("data-date");
        e.countdown(i).on("update.countdown", function (e) {
            $(this).html(
                e.strftime(
                    '<div class="countdown-s2-item">' +
                    '<span class="countdown-s2-time">%H</span> Giờ :&nbsp;</div> ' +
                    '<div class="countdown-s2-item">' +
                    '<span class="countdown-s2-time">%M</span> Phút :&nbsp;</div> ' +
                    '<div class="countdown-s2-item">' +
                    '<span class="countdown-s2-time ">%S</span> Giây </div>'
                )
            );
        });
    });
    var F = $(".countdown-s1");
    F.length > 0 &&
    F.each(function () {
        var e = $(this),
            i = e.attr("data-date");
        e.countdown(i).on("update.countdown", function (e) {
            $(this).html(
                e.strftime(
                    '<div class="countdown-item">' +
                    '<span class="countdown-time">%D</span> <span class="countdown-label"> Ngày </span></div> ' +
                    '<div class="countdown-item">' +
                    '<span class="countdown-time">%H</span>  <span class="countdown-label"> Giờ </span></div> ' +
                    '<div class="countdown-item">' +
                    '<span class="countdown-time ">%M</span>  <span class="countdown-label"> Phút </span></div>' +
                    '<div class="countdown-item">' +
                    '<span class="countdown-time ">%S</span>  <span class="countdown-label"> Giây </span></div>'
                )
            );
        });
    });

    $(".hv-swiper .swiper-slide").on("click", function (e) {
        e.preventDefault();
        $(this).siblings().removeClass("hv-active");
        $(this).addClass("hv-active");
        var num = ($(this).index() - 1) < 0 ? 0 : ($(this).index() - 1);
        hvSwiper.slideTo(num)
    });


    $(".des-nav-btn").on("click", function (e) {
        $(".des-nav-pos").toggleClass("is-active");
    });

    $(".des-nav-pos").on("click", function (e) {
        $(".des-nav-pos").removeClass("is-active");
    });


    $(".manual-device").each(function (index, element) {
        var $this = $(this);
        var numSlider = $this.attr("childId");
        var device = null;
        var process = null;

        if (numSlider != undefined) {
            device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
            process = $this
                .parent()
                .parent()
                .find(".manual-process#hd-sub-ctn-" + numSlider);
        } else {
            numSlider = index;
            device = $this.find(".manual-device-swiper");

            process = $this
                .parent()
                .parent()
                .find(".manual-process ");
        }

        var swiperArray = [];

        swiperArray[numSlider] = new Swiper(device, {
            observer: true,
            observeParents: true,
            on: {
                slideChangeTransitionEnd: function slideChangeTransitionEnd() {
                    var num = this.activeIndex;
                    process.children(".process_item").removeClass("active");
                    process
                        .children(".process_item")
                        .eq(num)
                        .addClass("active");
                }
            }
        });

        process.children(".process_item").each(function (index, element) {
            var num = index;
            $(element).on("click", function (e) {
                e.preventDefault();
                $(this)
                    .addClass("active")
                    .siblings()
                    .removeClass("active");
                swiperArray[numSlider].slideTo(index);
            });
        });
    });


    if ($('#dataTable1').length) {
        $('#dataTable1').DataTable(
            {
                "bFilter": false,
                "lengthChange": false,
                "info": false,
                "language": {
                    "decimal": "",
                    "emptyTable": "Không tim thấy kết quả",
                    "info": "Xem từ _START_ đến _END_ trong _TOTAL_ kết quả",
                    "infoEmpty": "Xem từ 0 đến 0 của 0 kết quả",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Xem _MENU_ kết quả",
                    "loadingRecords": "Chờ...",
                    "processing": "Chờ...",
                    "search": "Tìm kiếm:",
                    "zeroRecords": "Không tim thấy kết quả",
                    "paginate": {
                        "first": "Đầu",
                        "last": "Cuối",
                        "next": ">>",
                        "previous": "<<"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            }
        );
    }
    ;


    new fullpage('#fullpage', {
        scrollOverflow: true,
        // scrollOverflowReset: true,
        navigation: true,
        scrollingSpeed: 700,
        normalScrollElements: '.des-modal-scroll',
        // responsiveSlides:  true
        onLeave: function (origin, destination, direction) {


            $(".des-nav-item").removeClass("is-active");
            $(".des-nav-item[data-tar=" + destination.index + "]").addClass("is-active");

        }
    });


    $(".des-nav-item").on("click", function (e) {
        e.preventDefault();
        var act = $(this).data("tar") + 1;
        if (act !== 'undefined') {
            fullpage_api.moveTo(act);
            // desSwiper.slideTo(act);
        }
        return;
    });


    $("#des-btn-more").on("click", function (e) {
        e.preventDefault();
        fullpage_api.moveSectionDown();
    });


    $(".des-slideTop").on("click", function (e) {
        e.preventDefault();
        fullpage_api.moveTo(1);
    });


    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 100) {
            $(".lixi-nav2").addClass("is-active");
        } else {
            $(".lixi-nav2").removeClass("is-active");
        }
    });


    $('.des-modal-scroll').on('show.bs.modal', function (e) {
        fullpage_api.setAllowScrolling(false);
        fullpage_api.setKeyboardScrolling(false);

    })

    $('.des-modal-scroll').on('hidden.bs.modal', function (e) {
        fullpage_api.setAllowScrolling(true);
        fullpage_api.setKeyboardScrolling(true);
    })

});


var feedArray = [
    {"n": "hậu Nguyễn Phước", "t": 1},
    {"n": "Đoàn Hữu An", "t": 1},
    {
        "n": "Nguyễn Thắm",
        "t": 1
    },
    {"n": "hưng Nguyễn văn", "t": 1},
    {"n": "nguyen pham quoc hoa", "t": 1},
    {
        "n": " Bin",
        "t": 1
    }, {"n": "nguyễn thị như ý", "t": 1},
    {"n": "PhanPhan Dì", "t": 1}, {
        "n": "lương nguyễn thị hiền",
        "t": 1
    }
]


function ShowUserFeed(arrayAPI, rowNumber, prefixTxt) {
    let initArray = arrayAPI.slice(arrayAPI.length - rowNumber, arrayAPI.length);

    let feedUser = document.getElementById("livefeed");

    for (var i = 0; i < initArray.length; i++) {
        feedUser.innerHTML += "<div class='show'>" + initArray[i].n + " " + prefixTxt + "</div>";
    }

    let arrayLength = arrayAPI.length - rowNumber;

    setInterval(function () {

        arrayLength--;

        var userFeedItem = document.createElement("div");

        userFeedItem.innerHTML = arrayAPI[arrayLength].n + " " + prefixTxt;

        feedUser.insertBefore(userFeedItem, feedUser.firstChild);

        $('#livefeed > div').last().remove();

        setTimeout(function () {
            userFeedItem.className = userFeedItem.className + " show";
        }, 10);

        if (arrayLength === 0) arrayLength = arrayAPI.length;

    }, 2500)

}

// ShowUserFeed(feedArray, 5, "vừa tham gia");
